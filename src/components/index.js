// Cards
import ChartCard from "./Cards/ChartCard.vue";
import NavTabsCard from "./Cards/NavTabsCard.vue";
import StatsCard from "./Cards/StatsCard.vue";

// Tables
import NavTabsTable from "./Tables/NavTabsTable.vue";
import OrderedTable from "./Tables/OrderedTable.vue";
import TableAllPatients from "./Tables/TableAllPatients.vue";
import TableSickPatients from "./Tables/TableSickPatients.vue";
import TableHistory from "./Tables/TableHistory.vue";

export {
  ChartCard,
  NavTabsCard,
  StatsCard,
  NavTabsTable,
  OrderedTable,
  TableAllPatients,
  TableSickPatients,
  TableHistory
};
