import axios from 'axios'

export const patients = {
	state: {
		patients: [],
		sickPatients: [],
		history: []
	},
	mutations: {
		setPatients(state, payload) {
			payload.forEach(item => (state.patients.push({
				number: state.patients.length,
				picture: item.picture.thumbnail,
				name: item.name.first,
				id: item.id.value
			})))
		},
		overwritePatients(state){
			const arrPatients = []
			state.patients.forEach(item => (
				arrPatients.push({
					number: arrPatients.length,
					picture: item.picture,
					name: item.name,
					id: item.id
				})
			))
			state.patients = arrPatients
		},
		overwriteSickPatients(state){
			const arrSickPatients = []
			state.sickPatients.forEach(item => (
				arrSickPatients.push({
					number: arrSickPatients.length,
					picture: item.picture,
					name: item.name,
					id: item.id
				})
			))
			state.sickPatients = arrSickPatients
		},
		overwriteHistory(state) {
			const arrHistory = []
			state.history.forEach(item =>(
				arrHistory.push({
					number: arrHistory.length,
					picture: item.picture,
					name: item.name,
					sick: item.sick,
					identifier: item.identifier
				})
			))
			state.history = arrHistory
		},
		setSickPatient(state, payload) {
			state.sickPatients.push(payload)
			state.patients.splice(payload.number, 1)
			this.commit('overwritePatients')
			this.commit('overwriteSickPatients')
			let history = Object.assign({}, payload)
			history.sick = true
			history.identifier = history.id
			history.id = ''
			state.history.push(history)
			this.commit('overwriteHistory')
		},
		setHealthyPatient(state, payload) {
			state.sickPatients.splice(payload.number, 1)
			payload.number = state.patients.length
			state.patients.push(payload)
			this.commit('overwritePatients')
			this.commit('overwriteSickPatients')
			let history = Object.assign({}, payload)
			history.sick = false
			history.identifier = history.id
			history.id = ''
			state.history.push(history)
			this.commit('overwriteHistory')
		}
	},
	actions: {
		requestAllPatients({ commit }) {
			axios
				.get('https://randomuser.me/api/?results=100')
				.then(response => {
					commit('setPatients', response.data.results)
				})
		}
	},
	getters: {
		getPatients(state) {
			return state.patients
		},
		getSickPatients(state) {
			return state.sickPatients
		},
		getHistory(state) {
			return state.history
		},
		getHistorySick(state) {
			return state.history.filter(item => (item.sick === true))
		},
		getHistoryHealthy(state) {
			return state.history.filter(item => (item.sick === false))
		}
	}
}
