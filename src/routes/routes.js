import DashboardLayout from "@/pages/Layout/DashboardLayout.vue";

import Dashboard from "@/pages/Dashboard.vue";
import History from '@/pages/History.vue';
import HistorySick from '@/pages/HistorySick.vue';
import HistoryHealthy from '@/pages/HistoryHealthy.vue';

const routes = [
  {
    path: "/",
    component: DashboardLayout,
    redirect: "/dashboard",
    children: [
      {
        path: "dashboard",
        name: "Пациенты",
        component: Dashboard
      },
      {
        path: "history",
        name: "История",
        component: History
      },
      {
        path: "history/sick",
        name: "История заболевших",
        component: HistorySick
      },
      {
        path: "history/healthy",
        name: "История выздоровевших",
        component: HistoryHealthy
      }
    ]
  }
];

export default routes;
